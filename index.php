<?php 
	error_reporting( error_reporting() & ~E_NOTICE );
	session_start(); 
	require_once('includes/dbconnect.php');
	require_once('includes/dbdisconnect.php');
	require_once('includes/world.php');
	require_once('includes/phpResources.php');
	$loginStatus = $_SESSION['loginStatus'];
	if($loginStatus==1){header('location: includes/makeResume');}   
?>
<!DOCTYPE html>
<html>
<head>
	<meta  charset="utf-8" >
	<title>RESUME MAKER</title>
	<link rel="stylesheet" href="css/mainStyles.css">
    <script>
	   function delete_cookie( name ) {
		  document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	   }
	   function crCookie(name,value,time){
		   document.cookie = name + '=' + value + ';expires=' +time;
	   }
	   function getFields(){
		   var now = new Date();
		   var time = now.getTime();
		   var expireTime = time + 1000*5;//5s
		   now.setTime(expireTime);
		   var vvv = document.querySelector('[name="firstname"]').value;
		   crCookie('firstname',vvv,now.toGMTString() ); 
		   vvv = document.querySelector('[name="lastname"]').value;
		   crCookie('lastname',vvv,now.toGMTString() );
		   vvv = document.querySelector('[name="age"]').value;
		   crCookie('age',vvv,now.toGMTString() ); 
		   vvv = document.querySelector('[name="socialStatus"]').value;
		   crCookie('socialStatus',vvv,now.toGMTString() );
		   vvv = document.querySelector('[name="adress"]').value;
		   crCookie('adress',vvv,now.toGMTString() );
		   vvv = document.querySelector('[name="telephone"]').value;
		   crCookie('telephone',vvv,now.toGMTString() );
		   vvv = document.querySelector('[name="useremail"]').value;
		   crCookie('useremail',vvv,now.toGMTString() );
	   }
	   function getselection(obj){
		  var now = new Date();
		  var time = now.getTime();
		  var expireTime = time + 1000*60*5;//5min
		  now.setTime(expireTime);
		  crCookie(obj.name,obj.options[obj.selectedIndex].value,now.toGMTString() );
		  expireTime = time + 1000*5;//5s
		  now.setTime(expireTime);
		  crCookie('getselection',1,now.toGMTString() );
		  if(obj.name=='countryID'){
			  delete_cookie('stateID');
			  delete_cookie('cityID');
		  }
		  if(obj.name=='stateID'){
			  delete_cookie('cityID');
		  }
		  getFields();
		  location.reload();
	   }
      </script>
</head>
<body>
	<div id='outer'>
			<header id='top_header'>
				<hgroup>
					<h1><a href="index">RESUMÉ MAKER</a></h1>
				</hgroup>
					<nav id='top_nav'>
		              <?php echo displayFormLogin(); ?>
		            </nav>		
			</header>
			<div id=new_div>
					<div id='ddiv'>				
			        <p class='borderTest'>"Would you like me to give you a formula for success? It's quite simple, 
			        really: Double your rate of failure. You are thinking of failure as the enemy of success. But it isn't at all. 
			        You can be discouraged by failure or you can learn from it, so go ahead and make mistakes. Make all you can. 
			        Because remember that's where you will find success."</p>
					<br><img class='testImage' src='images/Untitled.png'>
			    </div>
				<div id='personalDiv'>
					<h2>Sign up it's Free :</h2>
					<form id='personalinfo' method='post' action='includes/register.php' accept-charset='utf-8'>
				        <p><label for='firstname'>First Name:</label>
						   <input type='text' name='firstname' placeholder='first name' value="<?php if(isset($_COOKIE['firstname'])) echo $_COOKIE['firstname']; ?>" autofocus />
						</p>
						<p><label for='lastname'>Last Name:</label>
						   <input type='text' name='lastname' placeholder='last name'  value="<?php if(isset($_COOKIE['lastname'])) echo $_COOKIE['lastname']; ?>"  />
						</p>				
						<p><label for='age'>Age:</label>
						   <input type='number' name='age' min='10' max='120'  value="<?php if(isset($_COOKIE['age'])) echo $_COOKIE['age']; ?>"   />
						   <label for='socialStatus'>&nbsp;&nbsp;Married:</label>
						   <input type='checkbox' name='socialStatus'  value="<?php if(isset($_COOKIE['socialStatus'])) echo $_COOKIE['socialStatus']; ?>"  />
						</p>	
						<p><label for='country'>Country:</label>
						       <select name='countryID' onchange='getselection(this)'>
							   <option value=''>Select Country</option>
							   <?php echo countries(); ?>
						   </select>
						   <label for='state'>State:</label>
						   <select name='stateID'  onchange='getselection(this)'>
							   <option value=''>Select State</option>
							   <?php if(isset($_COOKIE['countryID']) && $_COOKIE['countryID']) echo states($_COOKIE['countryID']); ?>
						   </select>
						   <label for='city'>City:</label>
						   <select name='cityID' onchange='getselection(this)'>
							  <option value=''>Select City</option>
							  <?php if(isset($_COOKIE['stateID']) && $_COOKIE['stateID']) echo cities($_COOKIE['stateID']); ?>
						   </select>
						</p>
						<p><label for='adress'>Adress:</label>
						   <input type='text' name='adress' placeholder='Adress'  value="<?php if(isset($_COOKIE['adress'])) echo $_COOKIE['adress']; ?>" />
						</p>
						<p><label for='telephone'>Telephone:</label>
						   <input type='tel' name='telephone'  value="<?php if(isset($_COOKIE['telephone'])) echo $_COOKIE['telephone']; ?>"  />
						</p>			
						<p><p><label for='useremail'>E-mail:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
						   <input type='email' name='useremail' placeholder='$fn'  value="<?php if(isset($_COOKIE['useremail'])) echo $_COOKIE['useremail']; ?>"  required /></p>
						   <p class='sessionmsg'><?php echo $_SESSION['message']; ?></p>
						</p>				
						<p><label for='password'>Password:&nbsp;</label>
						   <input type='password' name='password' placeholder='$fn'  required />
						</p>
						<p class='borderTest2'>Disclaimer: Our web site will make every effort to protect personal or business information you choose 
						to provide electronically in the same way it protects personal and business information provided through other methods.<br>
						This privacy statement is provided for informational purposes only. It is not meant to be a contract of any type.</p><br><br>						
						<p><input type='submit' name='submit1' value='Accept' /></p>			
					</form>				
		        </div>
		</div>			    
		<footer id='footer'>
			<small>Copyrights: &copy; 2016.</small>
			<?php dbdisconnect($db); ?>
	    </footer>
	</div>
</body>
</html>
