
	 
$(document).ready(function(){
	    // upload image when a file is selected	
		$('#fileToUpload').on('change',function(){
				$('#formUpload').submit();
		});
	    //select job title
	    $('#titleStatus').on('change',function(){			
				var x= document.getElementById('titleStatus').value;
				localStorage.setItem('selectedtem', x);
				$.post( "setTitleStatus.php", { v : x  } );
		});
		var v = localStorage.getItem('selectedtem');
		if(v!=undefined) $('#titleStatus').val(v).change();
		//auto complete for languages
		$('#ilanguage').autocomplete({
		    source:'EdAutoComplete.php', 
		    messages: {
	                    noResults: '',
	                    results: function() {}
	                  },
		    minLength:1
        });
});


function UpdateTechnicalKnowledge(){
	$('#teckForm').submit();
}

//accordion
$(function() {
	
		$( "#frameSection" ).accordion({				 
			 collapsible : true,
			 autoHeight  : false, 
			 animate     : 300,
			 heightStyle : 'content',
			 activate: function (event, ui) {	
				   var x = $(this).accordion("option", "active");
				   localStorage.setItem('saved_index', x);	
			 },
			 active : parseInt(localStorage.getItem('saved_index'))
		});
			
});

function makeIT() {
     getFields();
	 window.open('makeIT.php#zoom=160');
}

function deleteExperience(obj) {
	 var id   = obj.id;
	 var v = obj.name;
	 location.href='delexperience.php?id='+id +'&option='+v;
	 location.open();
}  

function deleteEducation(obj) {
	 var id   = obj.id;
	 var v = obj.name;
	 location.href='delEducation.php?id='+id +'&option='+v;
	 location.open();
} 

function deleteknowledge(obj) {
	 var id   = obj.id;
	 var v = obj.name;
	 location.href='deleteKnowledge.php?id='+id +'&option='+v;
	 location.open();
} 

function deleteLanguage(obj) {
	 var id   = obj.id;
	 var v = obj.name;
	 location.href='deleteLanguage.php?id='+id +'&option='+v;
	 location.open();
} 
     
function delete_cookie( name ) {
  document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
function crCookie(name,value,time){
   document.cookie = name + '=' + value + ';expires=' +time;
}
function getFields(){
   var now = new Date();
   var time = now.getTime();
   var expireTime = time + 1000*5;//5s
   now.setTime(expireTime);
   // personal fields:
   var vvv = document.querySelector('[name="firstname"]').value;
   crCookie('firstname',vvv,now.toGMTString() ); 
   vvv = document.querySelector('[name="lastname"]').value;
   crCookie('lastname',vvv,now.toGMTString() );
   vvv = document.querySelector('[name="age"]').value;
   crCookie('age',vvv,now.toGMTString() ); 
   vvv = document.querySelector('[name="socialStatus"]').value;
   crCookie('socialStatus',vvv,now.toGMTString() );
   vvv = document.querySelector('[name="adress"]').value;
   crCookie('adress',vvv,now.toGMTString() );
   vvv = document.querySelector('[name="telephone"]').value;
   crCookie('telephone',vvv,now.toGMTString() );
   vvv = document.querySelector('[name="email"]').value;
   crCookie('email',vvv,now.toGMTString() );
   vvv = document.querySelector('[name="countryID"]').value;
   crCookie('countryID',vvv,now.toGMTString() );
   vvv = document.querySelector('[name="stateID"]').value;
   crCookie('stateID',vvv,now.toGMTString() );
   vvv = document.querySelector('[name="cityID"]').value;
   crCookie('cityID',vvv,now.toGMTString() );
   //experiences:
   vvv = document.querySelector('[name="titleStatus"]').value;
   crCookie('jobTitle',vvv,now.toGMTString() );   
}

function getselection(obj){
  var now = new Date();
  var time = now.getTime();
  var expireTime = time + 1000*60*5;//5min
  now.setTime(expireTime);
  crCookie(obj.name,obj.options[obj.selectedIndex].value,now.toGMTString() );
  expireTime = time + 1000*5;//5s
  now.setTime(expireTime);
  crCookie('getselection',1,now.toGMTString() );
  if(obj.name=='countryID'){
	  delete_cookie('stateID');
	  delete_cookie('cityID');
  }
  if(obj.name=='stateID'){
	  delete_cookie('cityID');
  }
  getFields();
  location.reload();
}

// language level progress bar
function edImgSrc(img){
	var index = img.name;
	document.getElementById('fid5').LanguageLevel.value=index;
	var e = document.querySelectorAll('#langImg');
	for(var i=0 ;i<e.length;i++){
		if(i<=index) e[i].src ='../images/sFilled.png';		
		else e[i].src ='../images/sEmpty.png';
	}	
}

/*
var refreshSn = function ()
{
    var time = 600000; // 10 mins
    settimeout(
        function ()
        {
	        $.ajax({
	           url: 'refresh_session.php',
	           cache: false,
	           complete: function () {refreshSn();}
	        });
        },
        time
    );
};
*/
/*
    
var availableTags = [
	"ActionScript",
	"AppleScript",
	"Asp",
	"BASIC",
	"C",
	"C++",
	"Clojure",
	"COBOL",
	"ColdFusion",
	"Erlang",
	"Fortran",
	"Groovy",
	"Haskell",
	"Java",
	"JavaScript",
	"Lisp",
	"Perl",
	"PHP",
	"Python",
	"Ruby",
	"Scala",
	"Scheme"
];

*/

/*
window.onload = function setDataSource() {
	if (!!window.EventSource) {
		var source = new EventSource("msgProcess.php");
		source.addEventListener("message", function(e) {
			var chatroom = document.getElementById('chatroom'); 
	         chatroom.innerHTML += event.data + "<br>";		
			 chatroom.scrollTop = chatroom.scrollHeight;
		}, false);
	} 
}
*/

/*
$(document).ready(function($){
	/*
    $( "#technicalArea" ).autocomplete({
	    source: availableTags
     });
     */ /*
	var placeholder = 'c++;java;python;javascript;php;html5\nweb programming; xml ...';
	$('#technicalArea').attr('value', placeholder);
	
	$('#technicalArea').focus(function(){
	    if($(this).val() === placeholder){
	        $(this).attr('value', '');
	    }
	});
	
	$('#technicalArea').blur(function(){
	    if($(this).val() ===''){
	        $(this).attr('value', placeholder);
	    }    
	});
	
});

*/
$(document).load(function($){		
		var chatroom = document.getElementById('chatroom'); 	
        chatroom.scrollTop = chatroom.scrollHeight;		
});
	
// submit chat msg when press return
$(function () {
    $("#Chatmessage").keypress(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (!e.shiftKey && code == 13) {
			e.preventDefault();
            submitChatmessage();            
            return true;
        }
    });
    
});

function submitChatmessage() {
   var msg = $("#Chatmessage").val();	
   $("#Chatmessage").val('');
   $.post( "msgProcess.php", { Chatmessage : msg  } )
     .done(function( data ) {		 
		   //alert(data);           
   });
}

/*
$(function()
{
	if (window.EventSource == undefined){ return; }
	var source = new EventSource('msgProcess.php');	
	source.addEventListener("message", function (event) {
                var chatroom = document.getElementById('chatroom'); 
		         chatroom.innerHTML += event.data + "<br>";		
                 chatroom.scrollTop = chatroom.scrollHeight;
            }, false);
});
*/
