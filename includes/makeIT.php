<?php
session_start();
require_once('dbconnect.php');
require_once('phpResources.php');
$userImage = '../images/' .$_SESSION['USERIDENTIFIER'] .'.png';
$html = '
<html>
<head>
	<meta  charset="utf-8" >
	<link rel="stylesheet" href="../css/rStyles.css">
</head>
<body>
 <div class="head1">
    <img class="mphoto" src="' .$userImage .'" > 
    <p id="hh1">';
	  $html .= $_COOKIE['firstname'] . ' ' .$_COOKIE['lastname'];
	  $html .='<br><span id=hh01>';
	  $html .= getJobTitle($_COOKIE['jobTitle']);
	  $html .='</span>';
	  $html .='<br><span id=hh2>';
	  $html .= $_COOKIE['adress'] . ' ' .$_COOKIE['CITY'] .' ' .$_COOKIE['STATE'] . ', ' .$_COOKIE['COUNTRY'];
	  $html .= ' | tel: ' .$_COOKIE['telephone'] .' | E-mail: ' .$_COOKIE['email'];
	  $html .='</span>
    </p>
  </div>	
  <div class="head2">
    <header ><span>Experiences Professionnelles</span><hr></header>
';
             $str  ='<br>';
             $str .= getExperiences(0); 
             $str  = str_replace('<ul>',' ',$str);  $str = str_replace('</ul>','<br><br>',$str); 
             $str  = str_replace('<li id="mlistItem" >','',$str);  $str = str_replace('</li>','<br>',$str);
             $str  = nl2br($str);
             $html .= $str; 
             $html .='
  </div>
  <div class="head4">
    <br> 
     <header><span>Diplomes et Formations</span><hr></header>';
             $str  ='<br>';
             $str .= getEducation(0); 
             $str  = str_replace('<ul>',' ',$str);  $str = str_replace('</ul>','<br><br>',$str); 
             $str  = str_replace('<li id="mlistItem" >','',$str);  $str = str_replace('</li>','<br>',$str);
             $str  = nl2br($str);
             $html .= $str;             
             $html .='   
  </div>
  <div class="head3">
     <header><span>Connaissances Techniques</span><hr></header><br>';
             $html .= getKnowledge(0);
             $html .='<br><header><span>Langues</span><hr></header><br>';
             $html .= getLangauges(0);           
             $html .='   
  </div><br>

';
//echo $html;

//==============================================================
include("../mpdf60/mpdf.php");
$mpdf=new mPDF('s');
$mpdf->SetDisplayMode('fullpage');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
//==============================================================

?>
<?php
/*
//==============================================================

include("../mpdf60/mpdf.php");
$mpdf=new mPDF('c');
$mpdf->SetDisplayMode('fullpage');
// LOAD a stylesheet
$stylesheet = file_get_contents('mpdfstyleA4.css');
$mpdf->WriteHTML($stylesheet,1); // The parameter 1 tells that this is css/style only and nobody/html/text
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;



/*





<style>
.head1 {
 background-color:yellow;
 height:130px;
}

.photo{
       float:right;
       width:130px;
       height:130px;
}
h1 {
      font-family: sans-serif,Tahoma,Arial;
      font-size:2.5em;
      margin-top: 0px;
      margin-bottom: 0px;
}
h4 {
      font-family: sans;
      font-weight: bold;
      margin-top: 1em;
      margin-bottom: 0.5em;
}
div {
      margin-bottom: 1em;
      text-align:justify;
      background-color:beige;
}
.myfixed1 { position: absolute;
      overflow: visible;
      left: 0;
      bottom: 0;
      border: 1px solid #880000;
      background-color: #FFEEDD;
      background-gradient: linear #dec7cd #fff0f2 0 1 0 0.5;
      padding: 1.5em;
      font-family:sans;
      margin: 0;
}
.myfixed2 { position: fixed;
      overflow: auto;
      right: 0;
      bottom: 0mm;
      width: 65mm;
      border: 1px solid #880000;
      background-color: #FFEEDD;
      background-gradient: linear #dec7cd #fff0f2 0 1 0 0.5;
      padding: 0.5em;
      font-family:sans;
      margin: 0;
      rotate: 90;
}
</style>



<div class="myfixed1">

Block elements can be positioned alongside each other using the CSS property float: left or right. The
clear property can also be used, set as left|right|both. Float is only supported on block elements
(i.e. not SPAN etc.) and is not fully compliant with the CSS specification.
Float only works properly if a width is set for the float, otherwise the width is set to the maximum
available (full width, or less if floats already set).
<br />
Margin-right can still be set for a float:right and vice-versa.
<br />
A block element next to a float has the padding adjusted so that content fits in the remaining width.
Text next to a float should wrap correctly, but backgrounds and borders will overlap and/or lie under
the floats in a mess.
<br />
NB The width that is set defines the width of the content-box. So if you have two floats with width=50%
and either of them has padding, margin or border, they will not fit together on the page.
</div>
<div class="gradient" style="float: right; width: 28%; margin-bottom: 0pt; ">
his is text in a &lt;div&gt; element that is
set to float:right and width:28%. It also has an image with float:right inside. With this exception,
you cannot nest elements with the float property set inside one another.
</div>
<div style="clear: both; margin: 0pt; padding: 0pt; "></div>
This is text that follows a &lt;div&gt; element that is set to clear:both.
<h4>CSS "Position"</h4>
At the bottom of the page are two DIV elements with position:fixed and position:absolute set
<div class="myfixed1">1 Praesent pharetra nulla in turpis. Sed ipsum nulla, sodales nec, vulputate in,
scelerisque vitae, magna. Praesent pharetra nulla in turpis. Sed ipsum nulla, sodales nec, vulputate
in, scelerisque vitae, magna. Sed egestas justo nec ipsum. Nulla facilisi. Praesent sit amet pede quis
metus aliquet vulputate. Donec luctus. Cras euismod tellus vel leo. Sed egestas justo nec ipsum. Nulla
facilisi. Praesent sit amet pede quis metus aliquet vulputate. Donec luctus. Cras euismod tellus vel
leo.</div>
<div class="myfixed2">2 Praesent pharetra nulla in turpis. Sed ipsum nulla, sodales nec, vulputate in,
scelerisque vitae, magna. Sed egestas justo nec ipsum. Nulla facilisi. Praesent sit amet pede quis
metus aliquet vulputate. Donec luctus. Cras euismod tellus vel leo.</div>






*/
?>
