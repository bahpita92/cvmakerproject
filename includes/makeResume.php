<!DOCTYPE html>
<html>
<head>
	<meta  charset="utf-8" >
	<title>RESUMÉ MAKER</title>
	<link href="../css/resumeStyles.css" rel="stylesheet" type="text/css">	
	<link href="../jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet" >	
	
	
	<script src="../jscript/jquery-3.1.1.min.js" type="text/javascript" ></script>
	<script src="../jscript/jquery-3.1.1.js" type="text/javascript" ></script>
	<script src="../jquery-ui-1.12.1/external/jquery/jquery.js" type="text/javascript" ></script>
    <script src="../jquery-ui-1.12.1/jquery-ui.min.js" type="text/javascript" ></script>   
    <!--
    <script src="../jscript/js.cookie.js"></script> 
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
    <script src = "https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    -->   
	<script src="../jscript/myscripts.js"></script>  
	
	<?php session_start();
		error_reporting( error_reporting() & ~E_NOTICE );
		error_reporting( error_reporting() & ~E_WARNING );
		require_once('../includes/dbconnect.php');
		require_once('../includes/world.php');
		require_once('../includes/phpResources.php');
		require_once('../includes/processPersonal.php');
		require_once('../includes/dbdisconnect.php');
		$loginStatus = $_SESSION['loginStatus'];
		if(!$loginStatus){header('location: ../index.php');} 
    ?>
</head>
<body>
	<div id='outer'>
		<header id='top_header'>
			<hgroup>
				<h1><a href="../index">RESUMÉ MAKER</a></h1>
			</hgroup>
			<nav id='top_nav'>
				<ul>
					<li><a href='../includes/logout.php'>Logout</a></li>
					<li><a href='#'>Settings</a></li>
				</ul>
              
            </nav>		
		</header>
		<div id='inner'>
			<div id='frameSection'>
				<h2>Personal informations:</h2>
				<div id='div1'>					
					<?php echo makeForm(); ?>
				</div>
				<h2>Professional Experiences:</h2>
				<div id='div2'>					
				    <?php echo getExperiences(1); ?>
				    <br>	
				    <?php echo makeForm2('fid2'); ?>	
				    <br>  				
				</div>
				<h2>Education :</h2>
				<div id='div3'>					
				    <?php echo getEducation(1); ?>	
				    <?php echo makeForm3('fid3'); ?>	 						    			
				</div>	
				<h2>Technical knowledge :</h2>
				<div id='div4'>
				    <?php echo getKnowledge(1); ?>
					<br>	
					<?php echo makeForm4('fid4'); ?>	
					<br>  				    				    			
				</div>
				<h2>Languages :</h2>
				<div id='div5'>
				    <?php echo getLangauges(1); ?>
				   	<br>	
					<?php echo makeForm5('fid5'); ?>	
					<br>  	    				    			
				</div>
				<h2>Other :</h2>
				<div id='div6'>
				</div>														
			</div><!-- end frame section -->
	        <div id='leftSection'>
				<div class='userSpace'>
					<div class='imageZone'>
					   <?php  ob_start(); 
					          include 'getImageFromDb.php';
					          ob_get_clean(); 
					          $uimg= '../images/' .$userID .'.png';
					   ?>
					   <img class='userImage' src='<?php echo $uimg ; ?>'  alt='<?php echo 'No PHOTO in database. ' .$_SESSION['uploadMsg']; ?>' >
					   <form class='formUpload' name='formUpload' id='formUpload' action="uploadImage.php" method="POST" enctype="multipart/form-data">
				            <input value="Upload Image" class="ToUpload" name="fileToUpload" id="fileToUpload" type="file" >
				            <input type="submit" name="fileToUpload" value="Upload Image">
				       </form>
					</div>
					<div class='userZone'>
						<P class='myText'><?php echo "welcome " .$_SESSION['firstname'] ." " .$_SESSION['lastname'];?></P>
						<P><?php echo getSTATUS(); ?></P>
						<br><br>
						<p><button class='Mbutton' onclick="makeIT();">Make My Resumé</button></p>
						<p><small><i> </i></small></p>
					</div>				    
				</div>	
				<div class='Entertainment'>
					<br>
					<details open><summary >Professional Talk:</summary>	
						<p id='chatroom'>
						   <?php  echo getMessages(); ?>
						</p>
						<table id='chattable'>
							<tr>	
						       <td id='ChatmessageTD'><textArea id='Chatmessage' name='Chatmessage' ></textArea></td>
						       <td><button  id='submitChatmessage' onclick='submitChatmessage();' >Send</button></td>
						    </tr>
						</table><br><br>
					</details>
				</div>
		    </div><!--end left secion -->	
		</div><!--end innder secion -->	
		<article >
			<h3>Article of the day:</h3>
			<p class='myText2Col'> 
				<?php getArticle(); ?> 
			</p><br><br>
		</article>	
	    <br> 
	    <footer id='footer'>
		     <small>Copyrights: &copy; 2016 .</small>
		     <?php dbdisconnect($db); ?>
        </footer>
	</div><!--end outer secion -->
</body>
</html>
